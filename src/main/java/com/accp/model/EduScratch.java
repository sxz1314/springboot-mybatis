package com.accp.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "edu_scratch")
public class EduScratch {
    /**
     * scratch主键
     */
    @Id
    @Column(name = "scratch_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer scratchId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * scratch路径
     */
    @Column(name = "scratch_url")
    private String scratchUrl;

    /**
     * scratch图片路径
     */
    @Column(name = "scratch_pic")
    private String scratchPic;

    /**
     * 文件名字
     */
    @Column(name = "scratch_name")
    private String scratchName;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 1未删除，2已删除
     */
    private Boolean status;

    @Column(name = "scratch_video")
    private String scratchVideo;

    /**
     * 获取scratch主键
     *
     * @return scratch_id - scratch主键
     */
    public Integer getScratchId() {
        return scratchId;
    }

    /**
     * 设置scratch主键
     *
     * @param scratchId scratch主键
     */
    public void setScratchId(Integer scratchId) {
        this.scratchId = scratchId;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取scratch路径
     *
     * @return scratch_url - scratch路径
     */
    public String getScratchUrl() {
        return scratchUrl;
    }

    /**
     * 设置scratch路径
     *
     * @param scratchUrl scratch路径
     */
    public void setScratchUrl(String scratchUrl) {
        this.scratchUrl = scratchUrl;
    }

    /**
     * 获取scratch图片路径
     *
     * @return scratch_pic - scratch图片路径
     */
    public String getScratchPic() {
        return scratchPic;
    }

    /**
     * 设置scratch图片路径
     *
     * @param scratchPic scratch图片路径
     */
    public void setScratchPic(String scratchPic) {
        this.scratchPic = scratchPic;
    }

    /**
     * 获取文件名字
     *
     * @return scratch_name - 文件名字
     */
    public String getScratchName() {
        return scratchName;
    }

    /**
     * 设置文件名字
     *
     * @param scratchName 文件名字
     */
    public void setScratchName(String scratchName) {
        this.scratchName = scratchName;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取1未删除，2已删除
     *
     * @return status - 1未删除，2已删除
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置1未删除，2已删除
     *
     * @param status 1未删除，2已删除
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return scratch_video
     */
    public String getScratchVideo() {
        return scratchVideo;
    }

    /**
     * @param scratchVideo
     */
    public void setScratchVideo(String scratchVideo) {
        this.scratchVideo = scratchVideo;
    }
}