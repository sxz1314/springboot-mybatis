package com.accp.common.service;


import java.util.List;

/**
 * Created by 尚晓忠 on 2018\7\7 0007 23:54:35
 */
public interface IService<T> {
    T selectByKey(Object key);

    int save(T entity);

    int deleteByPrimaryKey(Object key);

    int updateAll(T entity);

    int updateNotNull(T entity);

    List<T> selectByExample(Object example);

    T selectOne(T entity);
    List<T> selectList(T entity);
    //TODO 其他...
}
