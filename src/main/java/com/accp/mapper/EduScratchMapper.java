package com.accp.mapper;

import com.accp.model.EduScratch;
import com.accp.util.MyMapper;

public interface EduScratchMapper extends MyMapper<EduScratch> {
}