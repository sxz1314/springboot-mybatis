package com.accp.util;

import java.io.Serializable;

/**
 * @description: 响应体
 * @author xxx
 * @version 1.0
 * @date 2018/12/11 17:16
 */
public class TradeMessages<T> implements Serializable {

	private static final long serialVersionUID = -2352663152701894912L;

	/**
	 * 请求用户ID
	 */
	private String userId;

	/**
	 * 签名
	 */
	private String sign;

	/**
	 * 请求返回码
	 */
	private String resultCode;

	/**
	 * 请求返回消息
	 */
	private String resultMessage;

	/**
	 *  请求的唯一编号，接口处理完业务后，将返回客户请求时传入的唯一编号
	 */
	private String requestUniqeld;
	/**
	 * 流水号
	 */
	private String serial;

	/**
	 * 返回值Json格式
	 */
	private T data;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public String getRequestUniqeld() {
		return requestUniqeld;
	}

	public void setRequestUniqeld(String requestUniqeld) {
		this.requestUniqeld = requestUniqeld;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
