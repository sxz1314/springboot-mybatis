package com.accp.service;

import com.accp.common.service.BaseService;
import com.accp.model.EduScratch;
import org.springframework.stereotype.Service;

/**
 * Created by 尚晓忠 on 2019/3/8 20:55:08
 */
@Service
public class ScratchService extends BaseService<EduScratch> {

}
