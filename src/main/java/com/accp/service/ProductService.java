package com.accp.service;

import com.accp.mapper.ProductMapper;
import com.accp.model.Product;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;
@Service
public class ProductService {
    @Autowired
    private ProductMapper productMapper;
    public List<Product> getList(Product product,int pageNum, int pageSize){
        Example example=new Example(Product.class);
        Example.Criteria criteria=example.createCriteria();
        if(StringUtil.isNotEmpty(product.getTitle())){
            criteria.andLike("title", "%" + product.getTitle()+ "%");
        }
        PageHelper.startPage(pageNum,pageSize);
        return  productMapper.selectByExample(example);
    }

}
