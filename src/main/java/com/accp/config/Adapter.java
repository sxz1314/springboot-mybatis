package com.accp.config;

import com.accp.Interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by 尚晓忠 on 2019/3/9 16:36:59
 */
@Configuration
public class Adapter extends WebMvcConfigurerAdapter {
    @Autowired
    private LoginInterceptor interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor).excludePathPatterns("/scratchList/**",
                "/v2/**",
                "/swagger-resources/**"
                ,"/login"
                ,"/reg");

    }
}
