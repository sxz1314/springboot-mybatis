package com.accp.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Created by 尚晓忠 on 2019/3/8 21:17:02
 */
@Component
@ConfigurationProperties
@Data
public class PubConfig {

    private String imageUploadPath;

}
